var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Promise = require('bluebird');
Promise.promisifyAll(mongoose);

var schema = new Schema({
   activity_date: {type: 'String', required: true},
   activity_info: {type: 'String', required: true}
});

var Activity = mongoose.model('Activity', schema);

module.exports = Activity;
