var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Promise = require('bluebird');
Promise.promisifyAll(mongoose);

var schema = new Schema({
   major_name: {type: 'String', required: true}
});

var Major = mongoose.model('Major', schema);

module.exports = Major;
