var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Promise = require('bluebird');
Promise.promisifyAll(mongoose);

var schema = new Schema({
   usertype_type: {type: 'String', required: true}
});

var UserType = mongoose.model('UserType', schema);

module.exports = UserType;
