var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Promise = require('bluebird');
Promise.promisifyAll(mongoose);

var schema = new Schema({
  post_user: {type: 'String', required: true},
  post_body: {type: 'String', required: true},
  post_date: {type: 'String', required: true}
});

var Post = mongoose.model('Post', schema);

module.exports = Post;
