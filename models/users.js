var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Promise = require('bluebird');
Promise.promisifyAll(mongoose);

var schema = new Schema({
   user_first_name: {type: 'String', required: true},
   user_last_name: {type: 'String', required: true},
   user_email: {type: 'String', required: true},
   user_password: {type: 'String', required: true},
   user_username: {type: 'String', required: true},
   user_date_created: {type: 'Date'},
   user_major: {type: 'String'},
   user_type: {type: 'String'},
   user_clubs: []
});

var User = mongoose.model('User', schema);

module.exports = User;
