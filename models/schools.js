var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Promise = require('bluebird');
Promise.promisifyAll(mongoose);
var Club = require('./clubs');

var schema = new Schema({
   school_name: {type: 'String', required: true},
   school_logo: {type: 'String', required: true},
   school_join_date : {type: 'Date'},
   school_clubs : []
});

var School = mongoose.model('School', schema);

module.exports = School;
