var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Promise = require('bluebird');
Promise.promisifyAll(mongoose);

var schema = new Schema({
  club_name: {type: 'String', required: true},
  club_leader: {type: 'String', required: true},
  club_secretary: {type: 'String'},
  club_treasurer: {type: 'String'},
  club_activities: [],
  club_posts: [],
  club_members: [],
  club_info: {type: 'String'},
  club_join_date: {type: 'Date'},
  club_pictures: []
});

var Club = mongoose.model('Club', schema);

module.exports = Club;
