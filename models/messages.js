var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Promise = require('bluebird');
Promise.promisifyAll(mongoose);

var schema = new Schema({
  message_subject: {type: 'String', required: true},
  message_body: {type: 'String', required: true},
  message_from_id: {type: 'String', required: true},
  message_to_id: {type: 'String', required: true},
  message_date_sent: {type: 'Date', required: true}
});

var Message = mongoose.model('Message', schema);

module.exports = Message;
