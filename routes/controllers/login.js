var express = require('express');
var router = express.Router();

var User = require('../../models/users');
var Major = require('../../models/majors');

/* Render Registration Page */
router.get('/register', function(req, res, next) {

    Major.findAsync().then(function(major_list) {
        console.log(major_list);
        res.render('register', {
            'major_list': major_list,
            'session': req.session
        });
    }).catch(next).error(console.error)

});

// router.post('/process_registration', function(req, res, next) {
//
//   var user = new User();
//   console.log('User Just Completed Registration');
//   var date =  new Date();
//
//   user.user_first_name = req.body.first_name;
//   user.user_last_name = req.body.last_name;
//   user.user_email = req.body.email;
//   user.user_password = req.body.password;
//   user.user_date_created = date;
//   user.user_major = req.body.major;
//   user.user_username = req.body.email;
//   user.user_type = req.body.type;
//
//
//   user.saveAsync()
//   .then(function(added) {
//     res.json({'status': 'success', 'User': added});
//   })
//   .catch(function(e) {
//       console.log("fail");
//       res.json({'status': 'error', 'error': e});
//   })
//   .error(console.error);
//
//
//
// });

router.get('/login', function(req, res, next) {
    res.render('login', {'session': req.session});
});

router.post('/process_login', function(req, res, next) {
    var post = req.body;
    console.log("=> USER ATTEMPTING TO LOGIN");
    console.log("=> USERNAME ENTERED: " + post.username);
    console.log("=> PASSWORD ENTERED: " + post.password)

    User.findOneAsync({"user_username": post.username, "user_password": post.password})
    .then(function(user) {
        if (!user) {
            res.send({"success": "invalid"});
        } else {
            if (post.password === user.user_password) {
                console.log('hi mom');
                req.session.user = user;
                res.send({"redirect": "/search_listing"});
            } else {
                res.send({"success": "invalid"});
            }
        }
    }).catch(next).error();
});

router.get('/logout', function(req, res, next) {
    req.session.reset();
    res.send({'redirect': '/logout'})
});

module.exports = router;
