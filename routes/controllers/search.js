var express = require('express');
var router = express.Router();

var User = require('../../models/users');
var Major = require('../../models/majors');
var School = require('../../models/schools');
var Club = require('../../models/clubs');

/* Render Search Page */
router.get('/search_listing', function(req, res, next) {

    School.findAsync().then(function(school_list) {
        console.log(school_list);
        res.render('school_listing', {
            'school_list': school_list,
            'session': req.session
        });
    }).catch(next).error(console.error)

});

router.get('/search_listing/:school_id', function(req, res, next) {
    //Get the SCHOOL
    School.findAsync({_id: req.params.school_id}).then(function(school) {
        console.log(school);
        //GRAB ALL ITS CLUBS
        Club.findAsync({
            _id: {
                $in: school[0].school_clubs
            }
        }).then(function(clubs) {
            console.log(clubs);

            //GET All Users
            User.findAsync({}, null, {
                sort: {
                    "_id": 1
                }
            }).then(function(all_users) {
                res.render('club_listing', {
                    'clubs': clubs,
                    'school': school[0],
                    'user_list': all_users
                });
            });
        })

    }).catch(next).error(console.error)

});

router.get('/all_clubs_listing', function(req, res, next) {
    Club.findAsync({}, null, {
        sort: {
            "_id": 1
        }
    }).then(function(clubs) {

        User.findAsync({}, null, {
            sort: {
                "_id": 1
            }
        }).then(function(all_users) {
            res.render('all_clubs_listing', {
                'club_list': clubs,
                'user_list': all_users
            });
        });

    }).catch(next).error(console.error)
});

router.get('/search_clubs', function(req, res) {
    res.render('search_clubs');
});

module.exports = router;
