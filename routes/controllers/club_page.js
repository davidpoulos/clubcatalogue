var express = require('express');
var router = express.Router();

var fs = require('fs');
var multer = require('multer');
var upload = multer({dest: 'upload/'});

var User = require('../../models/users');
var Major = require('../../models/majors');
var School = require('../../models/schools');
var Club = require('../../models/clubs');
var Activity = require('../../models/activities');
var Post = require('../../models/posts');

router.get('/view_club/:id', function(req, res) {
    //GET PAGE Club
    Club.findOneAsync({_id: req.params.id}).then(function(data) {
        //GET ALL Club Activities
        Activity.findAsync({
            _id: {
                $in: data.club_activities
            }
        }).then(function(activities) {

            //GET ALL Club Posts
            Post.findAsync({
                _id: {
                    $in: data.club_posts
                }
            }).then(function(posts) {

                //GET ALL Club Members
                User.findAsync({
                    _id: {
                        $in: data.club_members
                    }
                }).then(function(members) {

                    //GET All Users
                    User.findAsync({}, null, {
                        sort: {
                            "_id": 1
                        }
                    }).then(function(all_users) {
                        res.render('view_club', {
                            'club_data': data,
                            'session': req.session,
                            'activity_list': activities,
                            'post_list': posts,
                            'member_list': members,
                            'all_users_list': all_users
                        });
                    });
                });
            });
        });
    }).catch(function(e) {
        res.status(400).json({'status': 'fail', 'error': e});
    });
});

router.post('/process_activity', function(req, res, next) {
    var post = req.body;
    var date = req.body.activity_date;
    var body = req.body.activity_info;
    var club_id = req.body.activity_club_id;

    //INSERT Activity
    var activity = new Activity();
    activity.activity_date = date;
    activity.activity_info = body;
    activity.saveAsync().then(function(addedActivity) {
        console.log(addedActivity);
        //UPDATE CLUB Activities
        Club.updateAsync({
            _id: club_id
        }, {
            $push: {
                club_activities: addedActivity._id
            }
        }).then(function(data) {
            res.json(addedActivity);
        }).catch(function(e) {
            console.log("fail");
            res.json({'status': 'error', 'error': e});
        }).error(console.error);

    }).catch(function(e) {
        console.log("fail");
        res.json({'status': 'error', 'error': e});
    }).error(console.error);
});

router.post('/process_post', function(req, res, next) {

    var post = req.body;
    var club_id = req.body.post_club_id;

    var club_post = new Post();

    club_post.post_user = req.session.user._id;
    club_post.post_body = post.post_body;
    club_post.post_date = post.post_date;

    console.log(post);

    //INSERT PoST
    club_post.saveAsync().then(function(addedpost) {

        //ADD TO CLUB POSTS;
        Club.updateAsync({
            _id: club_id
        }, {
            $push: {
                club_posts: addedpost._id
            }
        }).then(function(data) {
            res.json(addedpost);
            console.log("SUCCESS: " + addedpost);
        }).catch(function(e) {
            console.log("fail");
            res.json({'status': 'error', 'error': e});
        }).error(console.error);
        //END OF UPDATE

    }).catch(function(e) {
        console.log("fail");
        res.json({'status': 'error', 'error': e});
    }).error(console.error);
});

router.post('/process_join', function(req, res, next) {
    var post = req.body;
    console.log("=> POST CLUB_ID: " + post.club_id);
    console.log("=> POST USER_ID: " + post.user_id);

    //UPDATE CLUB MEMBERS
    Club.updateAsync({
        _id: post.club_id
    }, {
        $push: {
            club_members: post.user_id
        }
    }).then(function(data) {
        console.log("=> SUCCESSFULLY UPDATED CLUB_MEMBERS W/ -> " + post.user_id);

        //UPDATE USER CLUBS
        User.updateAsync({
            _id: post.user_id
        }, {
            $push: {
                user_clubs: post.club_id
            }
        }).then(function(data) {
            console.log("=> SUCCESSFULLY UPDATED USER: " + post.user_id + "CLUBS");
        }).catch(function(e) {
            console.log("fail");
            res.json({'status': 'error', 'error': e});
        }).error(console.error);
        //END OF UPDATE

    }).catch(function(e) {
        console.log("fail");
        res.json({'status': 'error', 'error': e});
    }).error(console.error);

    //END OF UPDATE
    res.redirect('/view_club/' + post.club_id)
});

router.post('/process_leave', function(req, res, next) {

    var post = req.body;
    console.log("=> POST CLUB_ID: " + post.club_id);
    console.log("=> POST USER_ID: " + post.user_id);

    Club.updateAsync({
        _id: post.club_id
    }, {
        $pull: {
            club_members: post.user_id
        }
    }).then(function(data) {
        console.log("=> SUCCESSFULLY UPDATED CLUB_MEMBERS W/ -> " + post.user_id);

        //UPDATE USER CLUBS
        User.updateAsync({
            _id: post.user_id
        }, {
            $pull: {
                user_clubs: post.club_id
            }
        }).then(function(data) {
            console.log("=> SUCCESSFULLY UPDATED USER: " + post.user_id + "CLUBS");
        }).catch(function(e) {
            console.log("fail");
            res.json({'status': 'error', 'error': e});
        }).error(console.error);
        //END OF UPDATE

    }).catch(function(e) {
        console.log("fail");
        res.json({'status': 'error', 'error': e});
    }).error(console.error);

    res.redirect('/view_club/' + post.club_id)

});

var type = upload.single('image');
router.post('/upload_pic',type, function(req,res){
  var post = req.body;
  //UPLOAD IMAGE TO WORKING DIRECTORY;
  var file = "public/images/" + req.file.originalname;
  fs.readFile(req.file.path, function(err, data) {
      fs.writeFile(file, data, function(err) {
          if (err) {
              console.error(err);
              response = {
                  message: 'Sorry, file couldn\'t be uploaded.',
                  filename: req.file.originalname
              };
          } else {
              response = {
                  message: 'File uploaded successfully',
                  filename: req.file.originalname
              };
          }

          //FINISHED SAVING IMAGE TO DIRECTORY;
          Club.updateAsync({
              _id: post.club_id
          }, {
              $push: {
                  club_pictures: req.file.originalname
              }
          }).then(function(data) {
              console.log("=> SUCCESSFULLY UPDATED CLUB_PICTURES");

              res.redirect('/view_club/' + post.club_id)
            });
      });
  });
});


router.post('/delete_post', function(req,res){
  var post = req.body;

  Club.updateAsync({
      _id: post.club_id
  }, {
      $pull: {
          club_posts: post.post_id
      }
  }).then(function(data) {
    Post.findByIdAndRemoveAsync(post.post_id)
      .then(function(deleted) {
        res.json({'status': 'success', 'message': deleted});
      })
      .catch(function(e) {
        res.status(400).json({'status': 'fail', 'error': e});
      });
  });

});

router.post('/delete_activity', function(req,res){
  var post = req.body;

  Club.updateAsync({
      _id: post.club_id
  }, {
      $pull: {
          club_activities: post.activity_id
      }
  }).then(function(data) {
    Activity.findByIdAndRemoveAsync(post.activity_id)
      .then(function(deleted) {
        res.json({'status': 'success', 'message': deleted});
      })
      .catch(function(e) {
        res.status(400).json({'status': 'fail', 'error': e});
      });
  });

});

module.exports = router;
