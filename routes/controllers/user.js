var express = require('express');
var router = express.Router();

var User = require('../../models/users');
var Major = require('../../models/majors');
var School = require('../../models/schools');
var Club = require('../../models/clubs');

router.get('/user_profile', function(req, res) {

    User.findAsync({_id: req.session.user._id}).then(function(my_clubs) {
        Club.findAsync({
            _id: {
                $in: my_clubs[0].user_clubs
            }
        }).then(function(clubs) {
            //console.log(clubs);
            console.log(my_clubs);
            res.render('user_profile', {
                'session': req.session,
                'club_list': clubs
            });
        });
    });
});

router.post('/update_user', function(req, res) {
    var post = req.body;
    var message = "Successfully Updated";
    console.log(post);

    if (post.first_name == "" || post.last_name == "" || post.password == "") {
        message = "Unable to update";
        User.findAsync({_id: req.session.user._id}).then(function(my_clubs) {
            Club.findAsync({
                _id: {
                    $in: my_clubs[0].user_clubs
                }
            }).then(function(clubs) {
                //console.log(clubs);
                console.log(my_clubs);
                res.render('user_profile', {
                    'session': req.session,
                    'club_list': clubs,
                    'message': message
                });
            });
        });
    } else {
        User.updateAsync({
            _id: req.session.user._id
        }, {
            user_first_name: post.first_name,
            user_last_name: post.last_name,
            user_password: post.password
        }).then(function(data) {
            User.findAsync({_id: req.session.user._id}).then(function(my_clubs) {
                //RELOAD PAGE
                Club.findAsync({
                    _id: {
                        $in: my_clubs[0].user_clubs
                    }
                }).then(function(clubs) {
                    console.log(my_clubs);

                    User.findOneAsync({_id: req.session.user._id}).then(function(update_session){
                      req.session.user = update_session;
                      res.render('user_profile', {
                          'session': req.session,
                          'club_list': clubs,
                          'message': message
                      });
                    })


                });
            })
        })
    }
});



module.exports = router;
