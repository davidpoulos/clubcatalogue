var express = require('express');
var router = express.Router();

var fs = require('fs');
var multer = require('multer');
var upload = multer({dest: 'upload/'});

var User = require('../../models/users');
var Major = require('../../models/majors');
var School = require('../../models/schools');
var Club = require('../../models/clubs');

router.get('/add_school', function(req, res) {
    res.render('add_school');
});

/** Permissible loading a single file,
    the value of the attribute "name" in the form of "recfile". **/
var type = upload.single('image');
router.post('/process_new_school', type, function(req, res, next) {


  if(req.body.school_name != "")
  {



    //UPLOAD IMAGE TO WORKING DIRECTORY;
    var file = "public/images/" + req.file.originalname;
    fs.readFile(req.file.path, function(err, data) {
        fs.writeFile(file, data, function(err) {
            if (err) {
                console.error(err);
                response = {
                    message: 'Sorry, file couldn\'t be uploaded.',
                    filename: req.file.originalname
                };
            } else {
                response = {
                    message: 'File uploaded successfully',
                    filename: req.file.originalname
                };
            }

            //FINISHED SAVING IMAGE TO DIRECTORY;

            //SAVE SCHOOL TO MONGO.
            var today = new Date();
            var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
            var school = new School();
            console.log('Adding School');
            school.school_name = req.body.school_name;
            school.school_logo = req.file.originalname;
            school.school_join_date = date;
            school.saveAsync().then(function(addedSchool) {
                res.render('add_school', {'message': 'success'});
            }).catch(function(e) {
                console.log("fail");
                res.render('add_school', {'message': 'failure'});
            }).error(console.error);

        });
    });

  }else {
    res.render('add_school', {'message': 'Form not completed.'});
  }
});

router.get('/add_new_club', function(req, res, next) {
    //RETRIEVE ALL SCHOOLS
    School.findAsync({}, null, {
        sort: {
            "_id": 1
        }
    }).then(function(data) {
        //console.log(data);
        //LOAD FORM
        res.render('add_new_club', {
            'session': req.session,
            'school_list': data
        });
    }).catch(next).error(console.error)

});

router.post('/process_new_club', function(req, res, next) {
    //GRAB POST DATA
    var post = req.body;
    var school_id = req.body.school;
    var user_id = req.session.user._id;
    var club_description = req.body.club_desc;
    var club_name = req.body.club_name;


    if(school_id == "" || user_id == "" || club_description == "" || club_name == "")
    {
      School.findAsync({}, null, {
          sort: {
              "_id": 1
          }
      }).then(function(data) {
          //console.log(data);
          //LOAD FORM
          res.render('add_new_club', {
              'session': req.session,
              'school_list': data,
              'message': 'Missing form input.'
          });
      }).catch(next).error(console.error)
    }
    else {


    //TEST POST DATA
    console.log('=> ATTEMPING TO ADD NEW CLUB')
    console.log('=> SCHOOL_ID: ' + school_id);
    console.log('=> USER_ID: ' + user_id);
    console.log('=> CLUB_DESC: ' + club_description);

    //ADD NEW CLUB
    var club = new Club();
    club.club_name = club_name;
    club.club_leader = user_id;
    club.club_info = club_description;
    club.club_join_date = new Date();
    club.saveAsync().then(function(addedclub) {
        // res.json({'status': 'success', 'club': addedclub});
        console.log(addedclub);

        //UPDATE USER TO BE PART OF CLUB
        User.updateAsync({
            _id: user_id
        }, {
            $push: {
                user_clubs: addedclub._id
            }
        }).then(function(data) {
            console.log(data);
            res.json({'status': 'success', 'user_update': data});
        }).catch(function(e) {
            console.log("fail");
            res.json({'status': 'error', 'error': e});
        }).error(console.error);
        //END OF UPDATE User

        //UPDATE SCHOOL CLUB IS A PART OF
        School.updateAsync({
            _id: school_id
        }, {
            $push: {
                school_clubs: addedclub._id
            }
        }).then(function(data) {
            console.log(data);
            res.json({'status': 'success', 'school_update': data});
        }).catch(function(e) {
            console.log("fail");
            res.json({'status': 'error', 'error': e});
        }).error(console.error);
        //END OF UPDATE SCHOOL

    }).catch(function(e) {
        console.log("fail");
        res.json({'status': 'error', 'error': e});
    }).error(console.error);

    //DONE ADDING - RELOAD FORM
    School.findAsync({}, null, {
        sort: {
            "_id": 1
        }
    }).then(function(data) {
        //console.log(data);
        //LOAD FORM
        res.render('add_new_club', {
            'session': req.session,
            'school_list': data,
            'message': 'Club Successfully Added'
        });
    }).catch(next).error(console.error)

}

});

module.exports = router;
