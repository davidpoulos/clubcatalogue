var express = require('express');
var router = express.Router();

var Club = require('../models/clubs');

/* GET home page. */
router.get('/', function(req, res, next) {
  Club.findAsync()
    .then(function(clubinfo){
        res.render('index', {clubs: clubinfo});
    })
    .catch(next)
    .error(console.error)
});

module.exports = router;
