var express = require('express');
var router = express.Router();

var UserType = require('../../../models/usertypes');

router.route('/')
  .get(function(req, res, next) {
    console.log("Retrieving Types.");
    UserType.findAsync({}, null, {
        sort: {
            "_id": 1
        }
    })
    .then(function(data) {
      res.json(data);
    })
    .catch(next)
    .error(console.error)
  });

router.route('/')
  .post(function(req, res, next) {
    var userT = new UserType();
    console.log('Adding UserType');
    userT.usertype_type = req.body.type;
    userT.saveAsync()
    .then(function(added) {
      res.json({'status': 'success', 'UserType': added});
    })
    .catch(function(e) {
        console.log("fail");
        res.json({'status': 'error', 'error': e});
    })
    .error(console.error);
  });

router.route('/:id')
  .delete(function(req, res, next) {
    UserType.findByIdAndRemoveAsync(req.params.id)
      .then(function(deleted) {
        res.json({'status': 'success', 'user': deleted});
      })
      .catch(function(e) {
        res.status(400).json({'status': 'fail', 'error': e});
      });
  });


module.exports = router;
