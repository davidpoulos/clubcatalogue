var express = require('express');
var router = express.Router();

var Club = require('../../../models/clubs');

//Get all Clubs.
router.route('/').get(function(req, res, next) {
    Club.findAsync({}, null, {
        sort: {
            "_id": 1
        }
    }).then(function(clubs) {
        res.json(clubs);
    }).catch(next).error(console.error)
});

//Search for club
router
  .route('/search/:search_input')
    .get(function(req, res, next) {
      Club.findAsync(
        { $or:
          [ { "club_name": req.params.search_input },
            { "club_leader" : req.params.search_input}
          ]
      }).then(function(clubs) {
        res.json(clubs);
    }).catch(next).error(console.error)
    // res.json(req.params);
});

//Add a club.
router.route('/').post(function(req, res, next) {
    var club = new Club();
    console.log('here');
    club.club_name = req.body.name;
    club.club_leader = req.body.leader;
    club.club_secretary = req.body.secretary;
    club.club_treasurer = req.body.treasurer;
    club.saveAsync().then(function(addedclub) {
        res.json({'status': 'success', 'club': addedclub});
    }).catch(function(e) {
        console.log("fail");
        res.json({'status': 'error', 'error': e});
    }).error(console.error);
});

router.route('/update_club_activities').post(function(req, res, next) {
    var club_id = req.body.id;
    var activity_id = req.body.activityid;
    console.log('CLUB ID' + club_id);
    console.log('ACTIVITY ID' + activity_id);
    Club.updateAsync({
        _id: club_id
    }, {
        $push: {
            club_activities: activity_id
        }
    }).then(function(data) {
        console.log(data);
        res.json({'status': 'success', 'activity_update': data});
    }).catch(function(e) {
        console.log("fail");
        res.json({'status': 'error', 'error': e});
    }).error(console.error);
});

router
  .route('/update_club_posts').post(function(req, res, next) {
    var club_id = req.body.id;
    var post_id = req.body.activityid;
    console.log('CLUB ID' + club_id);
    console.log('POST ID' + post_id);
    Club.updateAsync({
        _id: club_id
    }, {
        $push: {
            club_posts: post_id
        }
    }).then(function(data) {
        console.log(data);
        res.json({'status': 'success', 'post_update': data});
    }).catch(function(e) {
        console.log("fail");
        res.json({'status': 'error', 'error': e});
    }).error(console.error);
});

router.route('/:id')
  .delete(function(req, res, next) {
    Club.findByIdAndRemoveAsync(req.params.id).then(function(deleted) {
        res.json({'status': 'success', 'note': deleted});
    }).catch(function(e) {
        res.status(400).json({'status': 'fail', 'error': e});
    });
});

router.route('/:id').get(function(req, res, next) {
    Club.findAsync({_id: req.params.id}).then(function(data) {
        res.json({'status': 'success', 'club': data});
    }).catch(function(e) {
        res.status(400).json({'status': 'fail', 'error': e});
    });
});

module.exports = router;
