var express = require('express');
var router = express.Router();

var User = require('../../../models/users');

router.route('/').get(function(req, res, next) {
    console.log("Retrieving users.");
    User.findAsync({}, null, {
        sort: {
            "_id": 1
        }
    }).then(function(data) {
        res.json(data);
    }).catch(next).error(console.error)
});

router.route('/:user_id').get(function(req, res, next) {
    User.findOneAsync({_id: req.params.user_id}).then(function(data) {
        res.json(data);
    }).catch(next).error(console.error)
});

router.route('/').post(function(req, res, next) {
    var user = new User();
    console.log('Adding User');
    user.user_first_name = req.body.first_name;
    user.user_last_name = req.body.last_name;
    user.user_email = req.body.email;
    user.user_password = req.body.password;
    user.user_date_created = req.body.date_created;
    user.user_major = req.body.major;
    user.user_username = req.body.username;
    user.user_type = req.body.type;
    user.saveAsync().then(function(added) {
        res.json({'status': 'success', 'User': added});
    }).catch(function(e) {
        console.log("fail");
        res.json({'status': 'error', 'error': e});
    }).error(console.error);
});

router.route('/update_user_clubs').post(function(req, res, next) {
    var user_id = req.body.user_id;
    var club_id = req.body.club_id;
    console.log('USER ID' + user_id);
    console.log('CLUB ID' + club_id);
    User.updateAsync({
        _id: user_id
    }, {
        $push: {
            user_clubs: club_id
        }
    }).then(function(data) {
        console.log(data);
        res.json({'status': 'success', 'user_update': data});
    }).catch(function(e) {
        console.log("fail");
        res.json({'status': 'error', 'error': e});
    }).error(console.error);
});

router.route('/:id').delete(function(req, res, next) {
    User.findByIdAndRemoveAsync(req.params.id).then(function(deleted) {
        res.json({'status': 'success', 'user': deleted});
    }).catch(function(e) {
        res.status(400).json({'status': 'fail', 'error': e});
    });
});

module.exports = router;
