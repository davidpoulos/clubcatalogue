var express = require('express');
var router = express.Router();

var Message = require('../../../models/messages');

router.route('/')
  .get(function(req, res, next) {
    console.log("Retrieving Messages.");
    Message.findAsync({}, null, {
        sort: {
            "_id": 1
        }
    })
    .then(function(data) {
      res.json(data);
    })
    .catch(next)
    .error(console.error)
  });

router.route('/')
  .post(function(req, res, next) {
    var msg = new Message();
    console.log('Adding Message');
    msg.message_subject = req.body.msg_subject;
    msg.message_body = req.body.msg_body;
    msg.message_to_id = req.body.to_id;
    msg.message_from_id = req.body.from_id;
    msg.message_date_sent = req.body.date;
    msg.saveAsync()
    .then(function(added) {
      res.json({'status': 'success', 'Message': added});
    })
    .catch(function(e) {
        console.log("fail");
        res.json({'status': 'error', 'error': e});
    })
    .error(console.error);
  });

router.route('/:id')
  .delete(function(req, res, next) {
    Message.findByIdAndRemoveAsync(req.params.id)
      .then(function(deleted) {
        res.json({'status': 'success', 'message': deleted});
      })
      .catch(function(e) {
        res.status(400).json({'status': 'fail', 'error': e});
      });
  });

module.exports = router;
