var express = require('express');
var router = express.Router();

var Post = require('../../../models/posts');

router.route('/')
  .get(function(req, res, next) {
    console.log("Retrieving Posts.");
    Post.findAsync({}, null, {
        sort: {
            "_id": 1
        }
    })
    .then(function(data) {
      res.json(data);
    })
    .catch(next)
    .error(console.error)
  });

router.route('/')
  .post(function(req, res, next) {
    var post = new Post();
    console.log('Adding Post');
    post.post_user = req.body.post_user;
    post.post_body = req.body.post_body;
    post.post_date = req.body.date;
    post.saveAsync()
    .then(function(added) {
      res.json({'status': 'success', 'Post': added});
    })
    .catch(function(e) {
        console.log("fail");
        res.json({'status': 'error', 'error': e});
    })
    .error(console.error);
  });

router.route('/:id')
  .delete(function(req, res, next) {
    Post.findByIdAndRemoveAsync(req.params.id)
      .then(function(deleted) {
        res.json({'status': 'success', 'message': deleted});
      })
      .catch(function(e) {
        res.status(400).json({'status': 'fail', 'error': e});
      });
  });

module.exports = router;
