var express = require('express');
var router = express.Router();

var Major = require('../../../models/majors');

router.route('/')
  .get(function(req, res, next) {
    console.log("Retrieving Majors.");
    Major.findAsync({}, null, {
        sort: {
            "_id": 1
        }
    })
    .then(function(data) {
      res.json(data);
    })
    .catch(next)
    .error(console.error)
  });

router.route('/')
  .post(function(req, res, next) {
    var major = new Major();
    console.log('Major');
    major.major_name = req.body.major;
    major.saveAsync()
    .then(function(added) {
      res.json({'status': 'success', 'Major': added});
    })
    .catch(function(e) {
        console.log("fail");
        res.json({'status': 'error', 'error': e});
    })
    .error(console.error);
  });

router.route('/:id')
  .delete(function(req, res, next) {
    Major.findByIdAndRemoveAsync(req.params.id)
      .then(function(deleted) {
        res.json({'status': 'success', 'major': deleted});
      })
      .catch(function(e) {
        res.status(400).json({'status': 'fail', 'error': e});
      });
  });

module.exports = router;
