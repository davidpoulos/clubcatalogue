var express = require('express');
var router = express.Router();

var Activity = require('../../../models/activities');

//Get all Activities.
router.route('/')
  .get(function(req, res, next) {
    console.log("Retrieving activities.");
    Activity.findAsync({}, null, {
        sort: {
            "_id": 1
        }
    })
    .then(function(activities) {
        res.json(activities);
    })
    .catch(next).error(console.error)
  });

// Add an activtity.
router.route('/')
  .post(function(req, res, next) {
    var activity = new Activity();
    console.log('here');
    activity.activity_date = req.body.date;
    activity.activity_info = req.body.info;
    activity.saveAsync()
    .then(function(addedActivity) {
        res.json({'status': 'success', 'activity': addedActivity});
    })
    .catch(function(e) {
        console.log("fail");
        res.json({'status': 'error', 'error': e});
    })
    .error(console.error);
});

//Remove an activity
router.route('/:id').delete(function(req, res, next) {
    Activity.findByIdAndRemoveAsync(req.params.id)
    .then(function(deletedActivity) {
        res.json({'status': 'success', 'activity': deletedActivity});
    })
    .catch(function(e) {
        res.status(400).json({'status': 'fail', 'error': e});
    });
  });

module.exports = router;
