var express = require('express');
var router = express.Router();

var School = require('../../../models/schools');

router.route('/')
  .get(function(req, res, next) {
    console.log("Retrieving schools.");
    School.findAsync({}, null, {
        sort: {
            "_id": 1
        }
    })
    .then(function(data) {
      res.json(data);
    })
    .catch(next)
    .error(console.error)
});

router.route('/')
  .post(function(req, res, next) {
    var school = new School();
    console.log('Adding School');
    school.school_name = req.body.name;
    school.school_logo = req.body.logo;
    school.school_join_date = req.body.date;
    school.saveAsync()
    .then(function(addedSchool) {
        res.json({'status': 'success', 'school': addedSchool});
    })
    .catch(function(e) {
        console.log("fail");
        res.json({'status': 'error', 'error': e});
    })
    .error(console.error);
});

router.route('/update_school_clubs')
  .post(function(req, res, next) {
    var school_id = req.body.id;
    var club_id = req.body.clubid;
     console.log('SCHOOL ID' + school_id);
     console.log('CLUB ID' + club_id);
     School.updateAsync(
     {_id: school_id},
     {$push: {school_clubs: club_id}}
    )
    .then(function(data) {
      console.log(data);
        res.json({'status': 'success', 'school_update':data});
    })
    .catch(function(e) {
        console.log("fail");
        res.json({'status': 'error', 'error': e});
    })
    .error(console.error);
});

router.route('/:id')
  .delete(function(req, res, next) {
    School.findByIdAndRemoveAsync(req.params.id)
      .then(function(deleted) {
        res.json({'status': 'success', 'school': deleted});
      })
      .catch(function(e) {
        res.status(400).json({'status': 'fail', 'error': e});
      });
  });

router.route('/:id')
  .get(function(req, res, next) {
    School.findAsync({_id: req.params.id})
      .then(function(deleted) {
        res.json({'status': 'success', 'school': deleted});
      })
      .catch(function(e) {
        res.status(400).json({'status': 'fail', 'error': e});
      });
  });





module.exports = router;
