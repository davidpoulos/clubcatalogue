//Created by: David Poulos
//Web App.
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var multer = require('multer');

//New Dependencies.
var mongoose = require('mongoose');
var session = require('client-sessions');

//Initialize Routers (Actual File).
var routes = require('./routes/index');

//Entire API.
var api = require('./routes/api/index');

// API Files.
var clubapi = require('./routes/api/clubs/api');
var activityapi = require('./routes/api/activities/api');
var schoolapi = require('./routes/api/schools/api');
var userapi = require('./routes/api/users/api');
var usertypesapi = require('./routes/api/usertypes/api');
var messagesapi = require('./routes/api/messages/api');
var postsapi = require('./routes/api/posts/api');
var majorsapi = require('./routes/api/majors/api');

//Controllers
var loginctrl = require('./routes/controllers/login');
var searchctrl = require('./routes/controllers/search');
var adminctrl = require('./routes/controllers/admin');
var clubpagectrl = require('./routes/controllers/club_page');
var userctrl = require('./routes/controllers/user');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

//pretty print json
app.set('json spaces', 2);


//Database
mongoose.connect('mongodb://localhost/clubcatalogue');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//Session Middleware
app.use(session({
  cookieName: 'session',
  secret: 'CITY_OF_RAPTURE',
  duration: 30 * 60 * 1000,
  activeDuration: 5 * 60 * 1000,
}));

app.use(function(req,res,next){
    res.locals.session = req.session;
    next();
});


//Activate routers (Endpoint).
app.use('/', routes);

//Entire API Endpoint (Documentation).
app.use('/api', api);

//API Endpoint.
app.use('/api/clubs', clubapi);
app.use('/api/activities', activityapi);
app.use('/api/schools', schoolapi);
app.use('/api/users', userapi);
app.use('/api/usertypes', usertypesapi);
app.use('/api/messages', messagesapi);
app.use('/api/posts', postsapi);
app.use('/api/majors', majorsapi);

//Controller Endpoints
app.use('/', loginctrl);
app.use('/', searchctrl);
app.use('/', adminctrl);
app.use('/', clubpagectrl);
app.use('/', userctrl);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.locals.pretty = true;
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  app.locals.pretty = true;
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
