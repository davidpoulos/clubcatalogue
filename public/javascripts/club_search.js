$(function() {

    $('#search').keypress(function(e) {
        if (e.which == 13) {

            $('#output').html('');
            $('#default').hide();

            $.ajax({
                type: "GET",
                url: '/api/clubs/search/' + $('#search').val(),
                dataType: 'json',
                success: function(data) {
                    console.log(data.length);

                    $.ajax({
                        type: "GET",
                        url: '/api/users',
                        dataType: 'json',
                        success: function(all_users) {

                            for (var i = 0; i < data.length; i++) {
                                var username = "";
                                for (user in all_users) {
                                    if (all_users[user]._id == data[i].club_leader) {
                                        username = all_users[user].user_first_name + " "
                                        + all_users[user].user_last_name;
                                    }
                                }

                                $('#output').append('<tr>' +
                                    '<td>' + data[i].club_name + '</td>' +
                                    '<td>' + username + '</td>' +
                                    '<td>' + data[i].club_info + '</td>' +
                                    '<td><a href="/view_club/' + data[i]._id + '">View Club Page</a></td>' + '</tr>');
                            }
                            if (data.length == 0) {
                                $('#output').append('<tr>' +
                                    '<td>No Results</td>' +
                                    '</tr>')
                                console.log('WORK?');
                            }
                        }
                    });
                }
            });
        }
    });
});
