$(function(){

  $('#logout').on('click', function(){
    $.ajax({
      'type': 'GET',
      'url' : '/logout',
      success : function(data){
        console.log('Successfully Logged Out');
        window.location.href = '/login';
      }
    });
  });
});
