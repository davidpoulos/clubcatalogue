$(function(){
  $('#submit_login').on('click', function(){
    var $user = $('#username');
    var $password = $('#password');

    console.log($user.val());
    console.log($password.val());

    $('#failure').hide();

    $.ajax({
          type: "POST",
          url: '/process_login',
          data: {
            'username': $user.val(),
            'password' : $password.val()
          },
          dataType: 'json',
          success: function(data) {
            console.log(data);
            if(data.success == 'invalid') {
              var failure = "<div class='red col s12 z-depth-1'>" +
                            " <h6 class='center-align' style='font-family: Futura; color: white;'>Invalid User Credentials.</h6>" +
                            "</div>";
              $('#failure').show();
              $('#failure').html(failure);
            } else {
              window.location.href = data.redirect;
            }
          },
      });


  });
});
