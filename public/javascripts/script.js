// var $ = require('jquery');

var addClub = function(){
  var $clubname = $('#clubname').val();
  var $clubleader = $('#leader').val();

  console.log($clubname);
  console.log($clubleader);

  $.ajax({
        type: "POST",
        url: '/api/clubs',
        data: {
            name: $clubname,
            leader: $clubleader
        },
        dataType: 'json',
        success: function(data) {
            $('#tablerows').append("<tr id='"+data.club._id+"'><td>"+data.club.club_name+"</td><td>"+data.club.club_leader+"</td><td><button class='btn-floating'><i class='material-icons'>delete</i></td></tr>");
            $('#clubname').val('');
            $('#leader').val('');
        }
    });

};

var delete_club = function(id, cb) {
    $.ajax({
        url: '/api/clubs/' + id,
        type: 'DELETE',
        data: {
            id: id
        },
        dataType: 'json',
        success: function(data) {
            cb();
        }
    });
};


$(function(){
  $('#subButton').on('click', addClub);
  $('#tablerows').on('click', 'tr td button', function() {
        var $this = $(this),
        $tr = $this.parent().parent(),
        id = $tr.attr('id');
        console.log($tr);
        delete_club(id, function() {
            $tr.remove();
        });
    });

    //MATERIALIZE SETTINGS
    $('.datepicker').pickadate({
      selectMonths: true, // Creates a dropdown to control month
      selectYears: 15 // Creates a dropdown of 15 years to control year
    });
    $('select').material_select();
    $(".button-collapse").sideNav();
});
