$(function(){

  $('select').material_select();

  $('#submit_button').on('click',function(e){
    console.log('requesting data');
    console.log($('#first_name').val());
    console.log($('#major').val());

    $('#success').hide();
    $.ajax({
          type: "POST",
          url: '/api/users',
          data: {
            'first_name': $('#first_name').val(),
            'last_name' : $('#last_name').val(),
            'email'     : $('#email').val(),
            'password'  : $('#password').val(),
            'major'     : $('#major').val(),
            'type'      : $('#type').val(),
            'username'  : $('#email').val().slice(0, $('#email').val().indexOf("@")),
            'date'      : new Date()
          },
          dataType: 'json',
          success: function(data) {
            console.log(data);
            var success = "<div class='green col s12 z-depth-1'>" +
                          " <h6 class='center-align' style='font-family: Futura; color: white;'>Account Successfully Created.</h6>" +
                          "</div>";
            $('#success').html(success);
            $('#success').show();
            $('#first_name').val("");
            $('#last_name').val("");
            $('#email').val("");
            $('#password').val("");
            $('#major').val("");
            $('#confirm_password').val("");
            $('#major').val("");
          }
      });
  });
});
