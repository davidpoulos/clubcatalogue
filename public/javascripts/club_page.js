
//CLUB LEADER FUNCTIONS
var add_activity = function(){
  var $activity_body = $('#activity_body');
  var $activity_date = $('#activity_date');
  var $activity_club_id = $('#activity_club_id');

  console.log('ACTIVITY DATE: ' + $activity_date.val());
  console.log('ACTIVITY BODY: ' + $activity_body.val());
  console.log('CLUB ID: ' + $activity_club_id.val());

  if($activity_body.val() != "" && $activity_date != "")
  {
    $.ajax({
        url: '/process_activity',
        type: 'POST',
        data: {
            'activity_date' : $activity_date.val(),
            'activity_info' : $activity_body.val(),
            'activity_club_id' : $activity_club_id.val()
        },
        dataType: 'json',
        success: function(data) {
            console.log(data);
            $('#activity_section').prepend('<div class="z-depth-2" id="activity_container">'+
                '<div id="info">'+
                  '<h6>About: </h6>'+
                  '<span style="color: white;">'+ data.activity_info +'</span>'+
                '</div>'+
                '<div id="date">'+
                '<h6>Date: </h6>'+
                '<span style="color: white;">'+ data.activity_date +'</span>'+
                '</div>'+
                '<div id="'+data._id+'">'+
                '<div class="btn red darken-4" id="delete_activity" >' +
                '<i class="material-icons">delete</i></button></div></div>' +
              '</div>');
            $activity_body.val('');
            $activity_date.val('');
        }
    });
  }

};

var add_post = function(){
  var $post_body = $('#post_body');
  var today = new Date().toJSON().slice(0,10).replace(/-/g,'/');
  var $post_club_id = $('#post_club_id');

  if($post_body.val() != "") {
    $.ajax({
      url : '/process_post',
      type: 'POST',
      data: {
        'post_body' : $post_body.val(),
        'post_date' : today,
        'post_club_id': $post_club_id.val()
      },
      success: function(data){
        $.ajax({
          url: '/api/users/' + data.post_user,
          type: 'GET',
          success: function(user_info) {
            $('#post_section').prepend('<div class="z-depth-2" id="post_container">'+
                '<div>'+
                '<h6>User: </h6>'+
                '<span style="color: white;">'+
                user_info.user_first_name + " " + user_info.user_last_name +'</span>'+
                '</div>'+
                '<div>'+
                '<h6>Post: </h6>'+
                '<span style="color: white;">'+
                data.post_body +'</span>'+
                '</div></div>');

            $post_body.val('');
          }
        });



      }
    });
  }
};


var delete_post = function(post_id, club_id, cb){
  $.ajax({
    url: '/delete_post',
    type: 'POST',
    data: {'club_id': club_id, 'post_id': post_id},
    success: function(data) {
      cb();
    }
  });
};

var delete_activity = function(activity_id, club_id, cb){
  $.ajax({
    url: '/delete_activity',
    type: 'POST',
    data: {'club_id': club_id, 'activity_id': activity_id},
    success: function(data) {
      cb();
    }
  });
};

var delete_p_html = function($html){
  $html.remove();
}

$(function(){

  //CLUB LEADER
  $('#submit_activity').on('click', add_activity);

  $('#post_section').on('click', 'div div div', function() {
        var $this = $(this),
            $div = $this.parent(),
            post_id = $div.attr('id');
            console.log("ID: " + post_id);
        var club_id = $('#d_post').val();
            console.log("CLUB ID: "+ club_id);
        delete_post(post_id,club_id, function(){
          delete_p_html($this.parent().parent());
          console.log($this.parent().parent());
        });
    });

    $('#activity_section').on('click', 'div div div', function() {
          var $this = $(this),
              $div = $this.parent(),
              activity_id = $div.attr('id');
              console.log("ID: " + activity_id);
          var club_id = $('#d_activity').val();
              console.log("CLUB ID: "+ club_id);
          delete_activity(activity_id,club_id, function(){
            delete_p_html($this.parent().parent());
            console.log($this.parent().parent());
          });
      });


  //CLUB MEMBER
  $('#submit_post').on('click', add_post);



  //OTHER
});
